# Template Mezzio Project Using PHP Community Edition or IBMDB2

## Running here
- [Customer List](http://isup04.speedware.com:8150/wsphp/cfenton/mezzio-test/public/customers)
- WebSmart Apache server
- /www/websmart/htdocs/wsphp/cfenton/mezzio-test/

## Old 
- [Customer List](http://itest3.freschesolutions.com:8150/wsphp/chelseatest/public/customers)

## Setup
- rename /config/autoload/local.php.dist to local.php
- rename either DB-pdo.php pf DB-ibmdb2.php to DB.php depending on your PHP install
- run composer update to get required /vendor files