<?php
/**
 * Database Connection
 * 
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      v1.0 - November 2019
 */

namespace App\Model;

use Laminas\Db\Adapter\Adapter;

class DB
{
    public $adapter;
    public $db2Connection;

    /**
     * invoke factory
     */
    public function __construct($i5_libl)
    {                
        // create the database connection
        $this->adapter = $this->setupConnection($i5_libl);

        // some stored procedures work better with a simple db2 connection, so create both
        $this->db2Connection = $this->setupDB2Connection($i5_libl);                            
    }
        
    /**
     * Get DB Adapter
     */
    public function getAdapter(): Adapter
    {
        return $this->adapter;
    }

    /**
     * Get DB2 Connection
     */
    public function getDB2Connection()
    {
        return $this->db2Connection;
    }

    /**
     * setup database connection
     */
    public function setupConnection($i5_libl)
    {
        $options = array("i5_naming" => DB2_I5_NAMING_ON, "i5_libl" => $i5_libl);

        return new \Laminas\Db\Adapter\Adapter(array(
            'driver' => 'IbmDb2',
            'platform' => 'IbmDb2',
            'persistent' => true,
            'platform_options' => array('quoted_identifiers' => false),
            'database' => '*LOCAL',
            'username' => '',
            'password' => '',
            'driver_options' => $options,
        ));
    }

    /**
     * setup database connection
     */
    public function setupDB2Connection($i5_libl)
    {
        $options = array("i5_naming" => DB2_I5_NAMING_ON, "i5_libl" => $i5_libl, 'cursor' => DB2_SCROLLABLE);

        return db2_connect('', '', '', $options);
    }
    
}
