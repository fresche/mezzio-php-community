<?php
/**
 * Program Call
 *
 * This class creates an instance of the toolkit and has a function which allows
 * you to call an RPG program.
 *
 * @package    SalesPortal
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      v1.0 - July 2018
 */

namespace App\Model;

class PgmCall
{
    public $tkobj;

    protected $eventManager;
    protected $param = array();
    protected $currentDs;

    public function __construct($connectionString)
    {
        $settings = [
            'stateless' => true,
            'plugSize' => '15M',
            'debug' => true,
            'debugLogFile' => '/www/websmart/htdocs/wsphp/chelseatest/log/toolkit_debug' . date("Ymd") . '.log', // upload to be local
            'XMLServiceLib' => 'QXMLSERV', // library of XMLSERVICE
            'HelperLib' => 'QXMLSERV',  // library of XMLSERVICE
        ];

        $this->setupToolkit($connectionString, $settings);
    }

    /**
     * setup toolkit
     */
    public function setupToolkit($connectionString, $settings)
    {        
        $this->tkobj = \ToolkitService::getInstance($connectionString, '', '', 'odbc');
        
        $this->tkobj->setOptions($settings);
    }

    /**
     * call program
     */
    public function callProgram($program, $lib, $parms)
    {
        $result = $this->tkobj->PgmCall($program, $lib, $parms, null, null);

        if ($result) {
            return $result['io_param'];
        }

        return false;
    }

    /**
     * add data structure
     */
    public function addDS($parms)
    {
        $this->param = array();
        foreach ($parms as $parm) {
            if (!isset($parm[self::VARYING])) {
                $parm[self::VARYING] = 'off';
            }
            $this->addParm($parm[self::ATTR], $parm[self::VAL], $parm[self::RETURN], $parm[self::TYPE], $parm[self::VARYING]);
        }

        return $this->param;
    }

    /**
     * add parameter
     */
    public function addParm($parmDesc, $parmValue, $rtnField, $type, $varying = 'off')
    {
        $parameter = null;

        $idx = strpos($parmDesc, 'A');
        if ($idx) {
            $parmlength = substr($parmDesc, 0, $idx);
            $parameter = $this->tkobj->AddParameterChar($type, $parmlength, $rtnField, $rtnField, $parmValue, $varying);
        }
        $idx = strpos($parmDesc, 'P');
        if ($idx) {
            $length = substr($parmDesc, 0, $idx);
            $dec = substr($parmDesc, $idx);
            $parameter = $this->tkobj->AddParameterPackDec($type, $length, $dec, $rtnField, $rtnField, $parmValue);
        }
        $idx = strpos($parmDesc, 'S');
        if ($idx) {
            $length = substr($parmDesc, 0, $idx);
            $dec = substr($parmDesc, $idx);
            $parameter = $this->tkobj->AddParameterZoned($type, $length, $dec, $rtnField, $rtnField, $parmValue);
        }
        $idx = strpos($parmDesc, 'B');
        if ($idx) {
            $parmlength = substr($parmDesc, 0, $idx);
            $parameter = $this->tkobj->AddParameterBin($type, $parmlength, $rtnField, $rtnField, $parmValue);
        }

        return $parameter;
    }

    /**
     * run command
     */
    public function runcmd($cmd)
    {
        $this->tkobj->ClCommand($cmd);
    }
}
