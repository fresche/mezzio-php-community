<?php
/**
 * Database Connection
 * 
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      v1.0 - November 2019
 */

namespace App\Model;

use Laminas\Db\Adapter\Adapter;

class DB
{
    public $adapter;
    public $db2Connection;
    public $defaults = [];
    public $tooklit;

    /**
     * invoke factory
     */
    public function __construct($i5_libl)
    {        
        $this->defaults = [                                    
            "pf_db2OdbcDsn" => "DSN=*LOCAL",
            "pf_db2OdbcLibraryList" => ";DBQ=," . $i5_libl,
            "pf_db2OdbcOptions" => ";NAM=1;TSFT=1", 
            "pf_db2OdbcUserID" => "*CURRENT",
            "pf_db2OdbcPassword" =>  "",                                   
            "pf_db2PDOOdbcOptions" => [],            
        ];

        // create the database connection
        $this->adapter = $this->setupConnection();

        // some stored procedures work better with a simple db2 connection, so create both
        $this->db2Connection = $this->setupDB2Connection();

        $this->pgmCall = $this->setupToolkit();                      
    }
    
    /**
     * Get Toolkit
     */
    public function getToolkit(): PgmCall
    {
        return $this->toolkit;
    }

    /**
     * Get DB Adapter
     */
    public function getAdapter(): Adapter
    {
        return $this->adapter;
    }

    /**
     * Get DB2 Connection
     */
    public function getDB2Connection()
    {
        return $this->db2Connection;
    }

    /**
     * setup database connection
     */
    public function setupConnection()
    {               
        return new \Laminas\Db\Adapter\Adapter(array(
            'dsn' => 'odbc:' . $this->defaults['pf_db2OdbcDsn'] . $this->defaults['pf_db2OdbcLibraryList'] . $this->defaults['pf_db2OdbcOptions'],
            'driver' => 'pdo',
            'platform' => 'odbc',
            'platform_options' => array('quote_identifiers' => false),
            'username' => $this->defaults['pf_db2OdbcUserID'],
            'password' => $this->defaults['pf_db2OdbcPassword'],
            'driver_options' => $this->defaults['pf_db2PDOOdbcOptions']
        ));        
    }

    /**
     * setup database connection
     */
    public function setupDB2Connection()
    {
        return new \PDO(
			'odbc:' . $this->defaults['pf_db2OdbcDsn'] . $this->defaults['pf_db2OdbcLibraryList'] . $this->defaults['pf_db2OdbcOptions'], 
			$this->defaults['pf_db2OdbcUserID'], 
			$this->defaults['pf_db2OdbcPassword'],
			$this->defaults['pf_db2PDOOdbcOptions']
		);
    }

    /**
     * Setup toolkit
     *
     * @return PgmCall
     */
    public function setupToolkit()
    {
        // get the toolkit to perform program calls
        $toolkit = new PgmCall($this->defaults['pf_db2OdbcDsn'] . $this->defaults['pf_db2OdbcLibraryList'] . $this->defaults['pf_db2OdbcOptions']);        

        // test toolkit
        //$dataArea = new \ToolkitApi\DataArea($toolkit->tkobj);        
        //$dataArea->setDataAreaName('PW_GRLS', 'XL_WEBLIB');        
        //$dataArea->readDataArea();  

        return $toolkit;
    }
}
