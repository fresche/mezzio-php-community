<?php
/**
 * Database Connection Factory
 *
 * @package    SalesPortal
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      v1.0 - July 2018
 */

namespace App\Model;

use Interop\Container\ContainerInterface;

class DBFactory
{
    public $adapter;
    protected $curJobUser;
    protected $defaultUser;

    /**
     * invoke factory
     */
    public function __invoke(ContainerInterface $container)
    {        
        $i5_libl = $container->get('config')['i5_libl'];

        return new DB($i5_libl);
    }

}
