<?php
/**
 * Customer Model Factory
 *
 * @package    SalesPortal
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      v1.0 - July 2018
 */

namespace App\Model;

use Interop\Container\ContainerInterface;

class CustomerModelFactory
{
    /**
     * invoke factory
     */
    public function __invoke(ContainerInterface $container): CustomerModel
    {        
        return new CustomerModel(
            $container->get('db')
        );
    }
}
