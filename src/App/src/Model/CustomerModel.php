<?php
/**
 * UI State Model
 *
 * @package    Curry Supply
 * @author     Fresche Soutions
 * @author     Chelsea Fenton <chelsea.fenton@freschesolutions.com>
 * @since      August 2019
 */
namespace App\Model;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Where;
use Laminas\Db\TableGateway\TableGateway;

class CustomerModel extends TableGateway
{
    protected $_tableName;
    protected $db;
    protected $_cols = [];

    /**
     * Class constructor
     * @param DB $db
     * @return parent::_construct
     */
    public function __construct($db)
    {
        $this->_tableName = 'MU_CUSTF';
        $this->db = $db->getAdapter();

        $this->_cols = [
            "custID" => "CMCUST",
            "custName" => "CMNAME",
        ];

        parent::__construct($this->_tableName, $this->db);
    }

    /**
     * Fetch all records selected based on $filters
     * @param string $userId User ID
     * @param string $gridId Key
     * @return array
     */
    public function fetchRecords($userId = null, $gridId = null, $uiStateKey = null)
    {
        $select = new Select();
        $select->columns(array_values($this->_cols));
        $select->from($this->_tableName);

        //$where = new Where();

        $result = $this->selectWith($select);

        $UIstates = array();

        if ($result) {
            // format returned data
            foreach ($result as $row) {

                /** array of trimmed row data */
                $UIstates[] = $row;
            }
        }

        return $UIstates;
    }


}
