<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\CustomerModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\JsonResponse;

class CustomerHandler implements RequestHandlerInterface
{
    private $customerModel;

    public function __construct(CustomerModel $customerModel)
    {
        $this->customerModel = $customerModel;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        // Create and return a response
        $urlParm = $request->getQueryParams()['id'] ?? 'No ID provided';
        $urlParm = htmlspecialchars($urlParm, ENT_HTML5, 'UTF-8');

        // TODO invoke my CustomerModel here. Look at the APC testhandler example
        // and see if I can get a DB connection going here

        $customers = $this->customerModel->fetchRecords();


        return new JsonResponse([
            'success' => 'Yeah Baby',
            'customers' => $customers,
        ]);
    }
}
