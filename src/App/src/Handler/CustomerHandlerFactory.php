<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\CustomerModelFactory;
use App\Model\CustomerModel;
use Psr\Container\ContainerInterface;

class CustomerHandlerFactory
{
    public function __invoke(ContainerInterface $container) : CustomerHandler
    {
        return new CustomerHandler($container->get(CustomerModel::class));
    }
}
