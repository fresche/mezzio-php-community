<?php

declare (strict_types = 1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\JsonResponse;

class HelloHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // Create and return a response
        // $target = $request->getQueryParams()['target'] ?? 'World';
        // $target = htmlspecialchars($target, ENT_HTML5, 'UTF-8');
        // return new HtmlResponse(sprintf(
        //     '<h1>Hello %s</h1>',
        //     $target
        // ));

        $urlParm = $request->getQueryParams()['id'] ?? 'No ID provided';
        $urlParm = htmlspecialchars($urlParm, ENT_HTML5, 'UTF-8');

        return new JsonResponse([
            'success' => 'Yeah Baby',
            'id' => $urlParm,
        ]);
    }
}
